# Duty Management Application

This application is a duty-shift management system for Residential Centers at Colleges/Universities who are looking for a better way to manage their information than the traditional pen-and-paper method.

## Problem Statement

As a Head Resident Assistant (HRA) with Bentley University’s Residential Center (Res Center) I know the troubles of selecting and recording duty selections first hand. The purpose of every residential center is to ensure the safety of its students. Most Res Centers do this by assigning Resident Assistants (RAs) duty shifts. A duty shift entails the RA being either in the office or doing rounds--walking around--the residence halls ensuring that students are safe and abiding by school policies.

Unfortunately, the assignment of duty shifts (picking duties) and the consolidation and record-keeping of duty shifts is a mundane process. 

Today, HRAs lead their staffs through duty selection using pen-and-paper methods like taking down names on a whiteboard calendar, and then aggregate an individual shift schedule for their staff. Once the staff schedule is created, it is sent to both the Residence Director (RD--one per staff) and the Res Center for wider distribution. The Res Center consolidates all the staff calendars into one master schedule that is sent to all Res Center staff.

## Project Goals

With the successful completion of this project, there will be no need to manually aggregate this information. Duty schedules will be created automatically from the input from HRAs, and duty switches will be instantly propagated through all schedules instead of having the need to go back and edit them all manually. This will increase the amount of time that Res Center Professional Staff (Pro Staff) have to work on other, more important things than simply assembling calendars.

## Milestones

## Technologies

### Laravel PHP Framework

[![Latest Stable Version](https://poser.pugx.org/laravel/framework/version.png)](https://packagist.org/packages/laravel/framework) [![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.png)](https://packagist.org/packages/laravel/framework) [![Build Status](https://travis-ci.org/laravel/framework.png)](https://travis-ci.org/laravel/framework)

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as authentication, routing, sessions, and caching.

Laravel aims to make the development process a pleasing one for the developer without sacrificing application functionality. Happy developers make the best code. To this end, we've attempted to combine the very best of what we have seen in other web frameworks, including frameworks implemented in other languages, such as Ruby on Rails, ASP.NET MVC, and Sinatra.

Laravel is accessible, yet powerful, providing powerful tools needed for large, robust applications. A superb inversion of control container, expressive migration system, and tightly integrated unit testing support give you the tools you need to build any application with which you are tasked.

### Laravel Official Documentation

Documentation for the entire framework can be found on the [Laravel website](http://laravel.com/docs).

#### Contributing To Laravel

**All issues and pull requests should be filed on the [laravel/framework](http://github.com/laravel/framework) repository.**

#### License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

