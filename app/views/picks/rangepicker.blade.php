@extends('layouts.main')

@section('header')
	<?php 
		// this determines how far to offset the first day of the month
		date_default_timezone_set('America/Toronto');
		if (isset($dutyDays[0])) {
			$startdate = new DateTime($dutyDays[0]->date);
			$offset = $startdate->format('w');
			$push = 14.25 * $offset;
		}

		$today = new DateTime();
	?>
	<style type="text/css">
		tr:nth-child(odd){
			background-color: #eeeeee;
		}
		.month{
			font-size: 32px;
		}
		.past{
			color:#a9a9a9;
		}
	</style>
@stop

@section('content')

<h1>Duty Days</h1>

@if(Auth::user()->fk_role > 1)

<div class="row">
    <div class="col-sm-5 alert alert-info">
        <h3>Pick Duty for {{$myStaff}}</h3>
              <select name="m" id="m">
	<!-- Change this for a for loop -->
	<option value="1" 	{{ ($today->format('n') == 1 )? 'selected' : '' }}>Jan</option>
	<option value="2"	{{ ($today->format('n') == 2 )? 'selected' : '' }}>Feb</option>
	<option value="3"	{{ ($today->format('n') == 3 )? 'selected' : '' }}>Mar</option>
	<option value="4"	{{ ($today->format('n') == 4 )? 'selected' : '' }}>Apr</option>
	<option value="5"	{{ ($today->format('n') == 5 )? 'selected' : '' }}>May</option>
	<option value="6"	{{ ($today->format('n') == 6 )? 'selected' : '' }}>Jun</option>
	<option value="7"	{{ ($today->format('n') == 7 )? 'selected' : '' }}>Jul</option>
	<option value="8"	{{ ($today->format('n') == 8 )? 'selected' : '' }}>Aug</option>
	<option value="9"	{{ ($today->format('n') == 9 )? 'selected' : '' }}>Sep</option>
	<option value="10"	{{ ($today->format('n') == 10) ? 'selected' : '' }}>Oct</option>
	<option value="11"	{{ ($today->format('n') == 11) ? 'selected' : '' }}>Nov</option>
	<option value="12"	{{ ($today->format('n') == 12) ? 'selected' : '' }}>Dec</option>
</select>

<select name="y" id="y">
	@foreach($years as $year)
		<option value="{{$year->year}}" {{ ($today->format('Y') == $year->year) ? 'selected' : '' }}>{{$year->year}}</option>
	@endforeach
</select>

<button class="btn btn-primary" id="go">Go</button>
    </div>
    <div class="col-sm-5 col-sm-offset-2 alert alert-danger">
        <h3>Semester Reset</h3>
        <button class="btn btn-danger" onclick="areYouSure({{Auth::user()->fk_staff}})">Reset duty counts</button>
    </div>
</div>
      
@endif


<div class="row">
	<div class="col-sm-6">
		<h3>Who's on Duty this Week?</h3>

			<table id="tab" cellpadding="12">
				<tr>
					<th style="text-align:right">Date</th>
					<th>RA(s)</th>
				</tr>
				@foreach($nextWeekDuty as $day)
					<?php
						$aday = new DateTime($day->date);
						$ras = $day->ras;
					?>
					<tr>
						<td width="25%" align="right">
							<span class="label label-primary" style="font-size:16px">
							<?php
								switch ($aday->format('d') - $today->format('d')) {
									case 0:
										echo "Today";
										break;
									case 1:
										echo "Tomorrow";
										break;
									default:
										echo $aday->format('l \t\h\e jS');
										break;
								}
							?>
							</span>
						</td>
						<td>
							@foreach($day->ras as $ra)
								{{ $ra['first_name'] }}	{{ $ra['last_name'] }}
								<br>
							@endforeach
						</td>
					</tr>
				@endforeach
			</table>
	</div>

	<div class="col-sm-6">
		<h3>My Duty Days <small><a href="/switches/create">Propose Switch</a></small></h3>
		<table class="table table-striped">
			<tr>
				<th>Date</th>
				<th>Partner</th>
			</tr>
			@foreach($myDutyDays as $d)
			<?php
				$aday = new DateTime($d->date);
			?>
			<tr>
				<td>
					<span class="{{ strtotime($d->date) < time() ? 'past' : '' }}">{{ $aday->format('l, F jS, Y'); }}</span>
				</td>
				<td><span class="{{ strtotime($d->date) < time() ? 'past' : '' }}">{{ $d->partner }}</span></td>
			</tr>
			@endforeach
		</table>
	</div>
</div>
@stop

@section('footer')
<!--
|-----------------------
| script to launch correct picker page
|-----------------------
-->
<script>
	$('#go').on('click', function(){
		var m = $('#m').val(),
			y = $('#y').val();

		console.log(m + '' + y);

		window.location.href = '/picks/' + m + '/' + y;
	});
	var areYouSure = function(s){
	    if(confirm('Are you sure you want to reset duty counts? This cannot be undone.')){
	        $.ajax({
                type: "POST",
                url: "/f/reset",
                data: { staff: s }
            })
            .success(function(){
                location.reload()
            });
        }
	    else
	        alert('no');
	};
</script>

@stop