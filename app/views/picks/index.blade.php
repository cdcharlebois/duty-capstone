@extends('layouts.main')

@section('header')
	<?php 
		// this determines how far to offset the first day of the month
		date_default_timezone_set('America/Toronto');
		$startdate = new DateTime($dutyDays[0]->date);
		$offset = $startdate->format('w');
		$push = 14.25 * $offset;
	?>
	<style type="text/css">
		.last + li{
			margin-left: {{ $push . '%' }}
		}
		.cal > li:not(.title){
			width:14.25%;
			float:left;
			display:inline;
			background-color:#eee;
			min-height:100px;
			border:1px solid #888;
			/*padding:3px;*/
		}
		.cal > li.title{
			width:14.25%;
			float:left;
			display:inline;
			background-color:#aaa;
			color:#fff;
			text-align: center;
		}
		.list > li{
			list-style-type: none;
		}
		.ra{
			-webkit-box-sizing:border-box !important;
			box-sizing:border-box !important;
			padding:3px !important;
			font-size: 16px;
			display:block !important;
			margin: 3px;
			/*background-color: #5CB85C;*/
			/*color:#fff;*/
			border-radius: 3px;
			text-align: center;
		}
		h4{
			background-color: #a9a9a9;
			color:#fff;
			margin:0;
			padding:3px;
		}
		.month{
			font-size: 32px;
		}
		.print-only{
			display:none !important;
		}
		@media print
		{
			hr{
				page-break-after:always;
			}
			.print-only{
				display:block !important;
			}
			.screen-only{
				display:none;
			}
		}
		/*Fixer for glyphicons*/
		.white{
			color:#ffffff;
			font-size: small;
			float:right;
		}
	</style>
@stop

@section('content')
	<?php
		$nextLink = $currMonth == 12 ? '/picks/1/' . ($currYear+1) : '/picks/' . ($currMonth+1) . '/' . $currYear;
		$prevLink = $currMonth == 1 ? '/picks/12/' . ($currYear-1) : '/picks/' . ($currMonth-1) . '/' . $currYear;
	?>
	<ul class="pager screen-only">
	  <li class="previous"><a href="{{ $prevLink }}">&larr; Previous Month</a></li>
	  <li class="month">
	  	<?php
			$mos = ['', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']; 
			echo "$mos[$currMonth], $currYear";
		?>
		<a class="btn" href="javascript:if(window.print)window.print()">Print</a>
	  </li>
	  <li class="next"><a href="{{ $nextLink }}">Next Month &rarr;</a></li>
	</ul>
		<div class="row">
			<div class="cal col-sm-7" style="min-height:550px">
				<!-- Use this for printing -->
				<div class="print-only" style="width:100%; clear:both; text-align:center">
					<h3>Bentley University Office of Residence Life</h3>
					<h3>{{ $staff->name }}</h3>
					<h3>RA Duty Calendar</h3>
					<p>Duty shifts begin at 7:30 PM and end at 8:30 AM the next day.</p>
					<h3>{{ $staff->name }} Duty Phone: {{ $staff->duty_phone }}</h3>
					<h3>Campus Police is always available at 781-891-3131</h3>
				</div>
				<li class="title">Sunday</li>
				<li class="title">Monday</li>
				<li class="title">Tuesday</li>
				<li class="title">Wednesday</li>
				<li class="title">Thursday</li>
				<li class="title">Friday</li>
				<li class="title last">Saturday</li>
				@foreach ($dutyDays as $day)
					<li {{($day->number_of_ras == 0 ? "style='background-color:rgb(255,100,100)'" : "")}}>
						{{ Form::open(['route' => 'picks.store']) }}
						{{ Form::hidden('userId', Session::get('picker')) }}
						{{ Form::hidden('dutyDayId', $day->id ) }}
						
							<?php 
								$foo = new DateTime($day->date);
							?>
							<h4>{{ $foo->format('d') }} <a class="screen-only" href="/dutydays/{{$day->id}}"><span class="glyphicon glyphicon-pencil white"></span></a></h4>
							
							<?php 
								$ras = $day->ras;
								$isWeekend = ($foo->format('w') == 6 || $foo->format('w') == 0);
							?>
							@foreach($day->ras as $ra)
								<div> 
									@if($day->oncall == 0 && $isWeekend)
										<a href="/picks/{{$day->id}}/oncall/{{$ra->id}}" class="ra label label-primary">{{ $ra->first_name }}</a>
									@else
									<div class="screen-only">
										@if($day->oncall == $ra->id)
											<span class="ra label label-warning">{{ $ra->first_name }}</span>
										@else
											<span class="ra label label-primary">{{ $ra->first_name }}</span>
										@endif
									</div>
									@endif
								</div>
								<div class="print-only">
									<table width="100%">
										<tr>
											<td style="font-weight:bold">{{ $ra->first_name }} <br>{{ $ra->last_name }}</td>
										</tr>
										<tr>
											<td>{{ $ra->room }}</td>
										</tr>
										<tr>
											<td>{{ $ra->phone }}</td>
										</tr>
									</table>
								</div>
							@endforeach
							@if (count($ras) < $day->number_of_ras)
								@if ($day->number_of_ras > 1 && Session::get('state') == 'WE')
									{{ Form::submit('pick &raquo;') }}
								@endif
								@if (Session::get('state') == 'WD')
									{{ Form::submit('pick &raquo;') }}
								@endif

							@endif
						<!-- </a> -->
						{{ Form::close() }}
					</li>
					<!-- Use this for the print view -->
					@if($foo->format('w') == 6)
						<!-- use this for printing -->
						<hr>
						<div class="print-only" style="width:100%; clear:both; text-align:center">
							<h3>Bentley University Office of Residence Life</h3>
							<h3>{{ $staff->name }}</h3>
							<h3>RA Duty Calendar, {{ $mos[$currMonth] . ', ' . $currYear }}</h3>
							<p>Duty shifts begin at 7:30 PM and end at 8:30 AM the next day.</p>
							<h3>{{ $staff->name }} Duty Phone: {{ $staff->duty_phone }}</h3>
							<h3>Campus Police is always available at 781-891-3131</h3>
						</div>
						<li class="print-only title">Sunday</li>
						<li class="print-only title">Monday</li>
						<li class="print-only title">Tuesday</li>
						<li class="print-only title">Wednesday</li>
						<li class="print-only title">Thursday</li>
						<li class="print-only title">Friday</li>
						<li class="print-only title">Saturday</li>
					@endif
				@endforeach
			</div>
			
			<hr class="print-only">

			<!-- LIST OF RAS -->
			<div class="list col-sm-5">
				<?php if (Session::get('state') != 'NONE'): ?>
				<h3>Now Picking: <span style="color:#D9534F">{{ $picker }}</span></h3>
				<?php endif ?>
				<table class="table table-striped">
					<tr>
						<th></th>
						<th>Name</th>
						@if(Session::get('state') === 'WE' || Session::get('state') === 'NONE')
						<th>Weekends</th>
						@endif
						@if(Session::get('state') === 'WD' || Session::get('state') === 'NONE')
						<th>Weekdays</th>
						@endif
						<th>Total</th>
					</tr>
				
				@foreach($picklist as $ra)
					<?php
						if (Session::get('state') === 'NONE'){
							$flag = false;
						}
						else{
							$flag = ($ra->id === Session::get('picker')); 	
						} 
						
					?>
					<tr>
						<td style="text-align:right">{{ $flag ? '<span class="label label-danger"><span class="glyphicon glyphicon-arrow-right"></span></span>' : '' }}</td>
						<td>{{ $ra->first_name . ' ' . $ra->last_name}}</span></td>
						@if(Session::get('state') === 'WE' || Session::get('state') === 'NONE')
						<td>{{ $ra->we_count }}</td>
						@endif
						@if(Session::get('state') === 'WD' || Session::get('state') === 'NONE')
						<td>{{ $ra->wd_count }}</td>
						@endif
						<td>{{ $ra->we_count + $ra->wd_count }}</td>
					</tr>
				@endforeach
				</table>
			</div>
		</div>
@stop
