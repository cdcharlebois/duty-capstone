@extends('layouts.main')

@section('content')
<div class="row">
	<div class="col-sm-6">
		{{ Form::open(['route' => 'roles.store']) }}
		<table class="table table-striped">
			<tr>
				<td>{{ Form::label('role_name', 'Role Name: ') }}</td>
				<td>{{ Form::text('role_name') }}</td>
			</tr>
			<tr>
				<td colspan="2">{{ Form::submit('Submit',['class' => 'btn btn-primary']) }}</td>
			</tr>
		</table>
		{{ Form::close() }}		
	</div>
</div>
@stop