@extends('layouts.main')

@section('content')
<div class="row">
	<div class="col-sm-6">
		<table class="table table-striped">
			<tr>
				<th>ID</th>
				<th>Role</th>
				<th>Edit</th>
			</tr>
			@foreach($roles as $role)
			<tr>
				<td>{{ $role->id }}</td>
				<td>{{ $role->role_name }}</td>
				<td><a href="/roles/{{$role->id}}/edit">Edit</a></td>
			</tr>
			@endforeach
			<tr>
				<td colspan="3">
					<a href="/roles/create" class="btn btn-success">Add Role</a>
				</td>
			</tr>
		</table>
	</div>
</div>
@stop


