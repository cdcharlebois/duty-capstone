@extends('layouts.main')

@section('content')
<div class="row">
	<div class="col-sm-6">
		<table class="table table-striped">
			<tr>
				<th>ID</th>
				<th>Role</th>
				<th colspan="2">Edit</th>
			</tr>
			<tr>
				<td>{{ $role->id }}</td>
				{{ Form::open(['url' => '/roles/' . $role->id, 'method'=>'put']) }}
				<td>{{ Form::text('role_name', $role->role_name) }}</td>
				<td>{{ Form::submit('Submit', ['class' => 'btn btn-primary']) }}</td>
				{{ Form::close() }}
				<td>
					{{ Form::open(['url' => '/roles/' . $role->id, 'method' => 'delete']) }}
						{{ Form::submit('Delete', ['class'=> 'btn btn-danger']) }}
					{{ Form::close() }}	
				</td>
			</tr>
		</table>
	</div>
</div>
@stop
