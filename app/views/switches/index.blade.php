@extends('layouts.main')

@section('content')
<h1>
	Available Switches <small>Can you take any of these days?</small> 
	@if(Auth::user()->fk_role > 1)
		<a class="btn btn-primary pull-right" href="/switches/accepted">
			Confirm Accepted Switches Here 
			@if ($numAccepted > 0)
				<span class="badge" style="background: #fff;color: #428bca;">{{ $numAccepted }}</span>
			@endif
			
		</a>
	@endif</h1>


<table class="table table-striped">
	<tr>
		<th>Date</th>
		<th>Partner</th>
		<th>Action</th>
	</tr>
	@foreach($dutydays as $day)
	{{ Form::open(['url' => '/switches/' . $day->id, 'method' => 'put']) }}
	{{ Form::hidden('to', Auth::user()->id) }}
	<?php
		$thisDay = new DateTime($day->date);
	?>
	<tr>
		<td>{{ $thisDay->format('l F jS'); }}</td>
		<td>{{ $day->partner }}</td>
		<td>{{ Form::submit('Take', ['class' => 'btn btn-primary']) }}</td>
	</tr>
	{{ Form::close() }}
	@endforeach
	<tr>
	    <td colspan="3">
         <div class="alert alert-success">
            Need someone to cover for you?
            <a href="/switches/create" class="btn btn-success">Submit a Request</a>    
         </div>
	    </td>
	</tr>
</table>

@stop