@extends('layouts.main')

@section('content')

<h1>Accepted Switches <small>To be Confirmed</small></h1>

<table class="table table-striped">
	<tr>
		<th>Date</th>
		<th>From</th>
		<th>To</th>
		<th>Action</th>
	</tr>
	@foreach($openSwitches as $s)
	<?php
	    $myDate = new DateTime($s->date);
	?>
	<!-- // [
		// 	{
		// 		->from_id
		// 		->from_name
		// 		->to_id 
		// 		->to_name
		// 		->day_id
		// 		->date
		// 	}
		// ] -->
	<tr>
		<td>{{ $myDate->format('l\, F jS'); }}</td>
		<td>{{ $s->from_name }}</td>
		<td>{{ $s->to_name }}</td>
		<td>
		    <a href="/switches/confirm/{{$s->id}}" class="text-success">Confirm</a> or 
		    <a href="/switches/deny/{{$s->id}}" class="text-danger">Deny</a>
        </td>
	</tr>
	@endforeach
</table>

@stop