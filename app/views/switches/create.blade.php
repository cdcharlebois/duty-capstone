@extends('layouts.main')

@section('content')

<h1>Hello {{ Auth::user()->first_name }}</h1>

<h3>Please select a duty day to offer</h3>

<table class="table table-striped">
	<tr>
		<th>Date</th>
		<th>Duty Partner</th>
		<th>Action</th>
	</tr>
	@foreach($dutydays as $day)
	{{ Form::open(['route' => 'switches.store']) }}
	{{ Form::hidden('from', Auth::user()->id) }}
	{{ Form::hidden('day', $day->fk_duty_day) }}
	<?php
		$thisDay = new DateTime($day->date);
	?>
	<tr>
		<td>{{ $thisDay->format('l\, F jS'); }}</td>
		<td>{{ $day->partner }}</td>
		<td>{{ Form::submit('Propose', ['class' => 'btn btn-primary']) }}</td>
	</tr>
	{{ Form::close() }}
	@endforeach
</table>

@stop