<html>
<head>
	<title></title>
	<link rel="stylesheet" href="/assets/lib/bootstrap/css/bootstrap.min.css">
	<script src="/assets/lib/jquery.js"></script>
	<script src="/assets/lib/moment.min.js"></script>
	<?php 
		// this determines how far to offset the first day of the month
		$startdate = new DateTime($dutyDays[0]->date);
		$offset = $startdate->format('w');
		$push = 14.25 * $offset;
	?>
	<style type="text/css">
		.last + li{
			margin-left: {{ $push . '%' }}
		}
		.cal > li:not(.title){
			width:14.25%;
			float:left;
			display:inline;
			background-color:#eee;
			height:125px;
			border:1px solid #888;
			/*padding:3px;*/
		}
		.cal > li.title{
			width:14.25%;
			float:left;
			display:inline;
			background-color:#aaa;
			color:#fff;
			text-align: center;
		}
		.list > li{
			list-style-type: none;
		}
		.ra-label{
			-webkit-box-sizing:border-box;
			box-sizing:border-box;
			padding:3px;
			font-size: 16px;
			background-color: #5CB85C;
			color:#fff;
			border-radius: 3px;
			text-align: center;
		}
		h4{
			background-color: #a9a9a9;
			color:#fff;
			margin:0;
			padding:3px;
		}
		.cal > li > * :not(h4){
			margin: 3px 6px;
		}
	</style>
</head>
<body>
	<div class="container">
		<h1>Duty Selection</h1>
		<div class="row">
			<div class="cal col-sm-7">
				<li class="title">Sunday</li>
				<li class="title">Monday</li>
				<li class="title">Tuesday</li>
				<li class="title">Wednesday</li>
				<li class="title">Thursday</li>
				<li class="title">Friday</li>
				<li class="title last">Saturday</li>
				@foreach ($dutyDays as $day)
					<li>
						{{ Form::open(['route' => 'picks.store']) }}
						{{ Form::hidden('userId', $picklist[Session::get('picker')]->_id) }}
						{{ Form::hidden('dutyDayId', $day->_id ) }}
						<!-- <a href="/pickday/{{$picklist[Session::get('picker')]->_id}}/{{$day->_id}}"> -->
							<?php 
								$foo = new DateTime($day->date);
							?>
							<h4>{{ $foo->format('d') }}</h4>
							<!-- <span class="label label-default"> Needed: {{ $day->number_of_ras }} </span> <br> -->
							<!-- <span class="label label-success">ID: {{ $day->_id }}</span> -->
							
							<?php 
								$ras = $day->ras;
							?>
							@foreach($day->ras as $ra)
								<div class="ra-label">{{ $ra['first_name'] }}</div>
							@endforeach
							@if (count($ras) < $day->number_of_ras)
								{{ Form::submit('pick &raquo;') }}
							@endif
						<!-- </a> -->
						{{ Form::close() }}
					</li>
				@endforeach
			</div>
			

			<!-- LIST OF RAS -->
			<div class="list col-sm-5">
				<table class="table table-striped">
					<tr>
						<th></th>
						<th>Name</th>
						<th>Count</th>
					</tr>
				
				@foreach($picklist as $ra)
					<?php $flag = ($ra == $picklist[Session::get('picker')]) ?>
					<tr>
						<td style="text-align:right">{{ $flag ? '<span class="label label-danger"><span class="glyphicon glyphicon-arrow-right"></span></span>' : '' }}</td>
						<td>{{ $ra->first_name . ' ' . $ra->last_name}}</span></td>
						<td>{{ $ra->count }}</td>
					</tr>
				@endforeach
				</table>
			</div>
		</div>
	</div>
	


	


</body>
<script>
	
</script>
</html>
