<html>
<head>

	<title>OnRounds Duty Management</title>
	<meta name="viewport" content="initial-scale=1">
	<link rel="stylesheet" href="/assets/lib/bootstrap/css/bootstrap.min.css">
	<link href='http://fonts.googleapis.com/css?family=Krona+One' rel='stylesheet' type='text/css'>
	<script src="/assets/lib/jquery.js"></script>
	<script src="/assets/lib/bootstrap/js/bootstrap.min.js"></script>
	@yield('header')
	<style>
		body{
			padding-top:70px;
		}
		/*colors
		http://colourco.de/complement/5/%234bb0c3*/
		.logo-on{
			font-family: 'Krona One', sans-serif;
			color: #48acc0;
		}
		.logo-rounds{
			font-family: 'Krona One', sans-serif;
			color: 	#b85644;
		}
	</style>
</head>
<body>
	<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<a href="/" class="navbar-brand">
				<span class="logo-on">on</span><span class="logo-rounds">rounds</span>
			</a>
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
			        <span class="sr-only">Toggle navigation</span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			      </button>
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li><a href="/users">Users</a></li>
					<li><a href="/picks">Duty Days</a></li>
					<li><a href="/switches">Switches 
					    @if( Auth::user() && Session::get('badge') > 0 )
					    <span class="badge">{{ Session::get('badge') }}</span>
				        @endif
				    </a></li>
					<li><a href="/me">My Information</a></li>
					@if( Auth::user() && Auth::user()->fk_role == 5 )
					<li><a href="/admin">Admin</a></li>
					@endif
				</ul>
				@if( Auth::user() )
					<a class='btn navbar-btn btn-default navbar-right' href='/logout'>Logout</a>
					<p class="navbar-text navbar-right">Currently logged in as <b> {{ Auth::user()->first_name . ' ' . Auth::user()->last_name }} </b>&nbsp;</p>
					
				@else
					<a class="btn navbar-btn btn-default navbar-right" href='/login'>Login</a>
		
				@endif
			</div>
		</div>
	</nav>
	@if ( Session::has('message') )
		<div class="alert alert-{{Session::get('message')['type']}}" style="text-align:center">
			{{Session::get('message')['text']}}
		</div>
	@endif
	<div class="container">
		@yield('content')
		<nav class="navbar navbar-default navbar-fixed-bottom" role="navigation">
            <div class="container" style="text-align:center">
                <p class="navbar-text" style="width:100%">Onrounds is a work in progress. Please help the development effort by emailing any bugs to Conner Charlebois.</p>
            </div>
        </nav>
	</div>
</body>
@yield('footer')
</html>
