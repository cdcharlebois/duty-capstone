@extends('layouts.main')

@section('content')
	<?php
		$today = new DateTime();
	?>
	<!-- frame -->
	<div class="row">
		<!-- left navbar -->
		<div class="col-sm-3">
			<h1>Hello {{ Auth::user()->first_name }}</h1>
			<ul class="nav nav-pills nav-stacked">
			  <li class="active"><a href="#master" data-toggle="tab">Master Schedule</a></li>
			  <li><a href="#staffs" data-toggle="tab">Staffs</a></li>
			  <li><a href="#users" data-toggle="tab">Users</a></li>
			  <li><a href="#roles" data-toggle="tab">Roles</a></li>
			</ul>
		</div>
		<!-- /left navbar -->
		<!-- main content -->
		<div class="col-sm-9">
			<!-- Tab panes -->
			<div class="tab-content">
			  <!-- tab 1 -->
			  <div class="tab-pane active" id="master">
			  	<h6>Pick a month and year to view the Master Calendar</h6>
			  	<select name="m" id="m">
			  		<!-- Change this for a for loop -->
			  		<option value="1" 	{{ ($today->format('n') == 1 )? 'selected' : '' }}>Jan</option>
			  		<option value="2"	{{ ($today->format('n') == 2 )? 'selected' : '' }}>Feb</option>
			  		<option value="3"	{{ ($today->format('n') == 3 )? 'selected' : '' }}>Mar</option>
			  		<option value="4"	{{ ($today->format('n') == 4 )? 'selected' : '' }}>Apr</option>
			  		<option value="5"	{{ ($today->format('n') == 5 )? 'selected' : '' }}>May</option>
			  		<option value="6"	{{ ($today->format('n') == 6 )? 'selected' : '' }}>Jun</option>
			  		<option value="7"	{{ ($today->format('n') == 7 )? 'selected' : '' }}>Jul</option>
			  		<option value="8"	{{ ($today->format('n') == 8 )? 'selected' : '' }}>Aug</option>
			  		<option value="9"	{{ ($today->format('n') == 9 )? 'selected' : '' }}>Sep</option>
			  		<option value="10"	{{ ($today->format('n') == 10) ? 'selected' : '' }}>Oct</option>
			  		<option value="11"	{{ ($today->format('n') == 11) ? 'selected' : '' }}>Nov</option>
			  		<option value="12"	{{ ($today->format('n') == 12) ? 'selected' : '' }}>Dec</option>
			  	</select>

			  	<select name="y" id="y">
			  		@foreach($years as $year)
			  			<option value="{{$year->year}}" {{ ($today->format('Y') == $year->year) ? 'selected' : '' }}>{{$year->year}}</option>
			  		@endforeach
			  	</select>

			  	<button class="btn btn-primary" id="go">Go</button>

			  </div>
			  <!-- /tab -->

			  <!-- tab 2 -->
			  <div class="tab-pane" id="staffs">
			  	<table class="table table-striped table-hover">
			  		<tr>
			  			<th>Name</th>
			  			<th>Duty Phone</th>
			  			<th>Edit</th>
			  		</tr>
			  		@foreach($staffs as $s)
			  		<tr>
			  			@if( isset($edit_staff) && $s->id == $edit_staff )
			  				{{ Form::open(['url' => "/staffs/$s->id", 'method' => 'put']) }}
			  					<td>{{ Form::text('name', $s->name) }}</td>
			  					<td>{{ Form::text('phone', $s->duty_phone) }}</td>
			  					<td>{{ Form::submit('Update', ['class' => 'btn btn-info']) }}</td>
			  				{{ Form::close() }}
			  			@else
			  				<td>{{ $s->name }}</td>
			  				<td>{{ $s->duty_phone }}</td>
			  				<td>
			  					<a href="/admin/staffs/edit/{{$s->id}}#staffs">
			  						<span class="glyphicon glyphicon-pencil"></span>
			  					</a>
			  				</td>
			  			@endif

			  		</tr>
			  		@endforeach
			  		<tr>
			  			{{ Form::open(['url' => '/staffs', 'method' => 'post']) }}
			  			<td>{{ Form::text('name', '', ['placeholder' => 'Staff Name']) }}</td>
			  			<td>{{ Form::text('phone', '', ['placeholder' => 'Duty Phone']) }}</td>
			  			<td>{{ Form::submit('Add Staff',['class' => 'btn btn-success']) }}</td>
			  			{{ Form::close() }}
			  		</tr>
			  	</table>
			  </div>
			  <!-- /tab 2 -->
				

			  <!-- tab 3 - EDIT USERS -->
			  <div class="tab-pane" id="users">
			  	
			  	<table class="table table-striped table-hover">
			  		<tr>
			  			<th>Name</th>
			  			<th>Role</th>
			  			<th>Staff</th>
			  			<th>Contact</th>
			  			<th>Edit</th>
			  		</tr>
			  		@foreach($ras as $ra)
			  		
			  		<!-- If this RA is being edited, show a pre-populated form -->
			  		@if (isset($edit) && $edit == $ra->id)
			  		<tr>
			  			{{ Form::open(['url' => '/users/' . $ra->id, 'method' => 'put']) }}
			  			<td>
			  				{{ Form::text('first_name', $ra->first_name) }}
			  				{{ Form::text('last_name', $ra->last_name) }}
			  			</td>
			  			<td>
							<select name="role" id="role">
								@foreach($roles as $role)
									<option value="{{ $role->id }}" {{ $role->id === $ra->fk_role ? "selected" : "" }}>{{ $role->role_name }}</option>
								@endforeach
							</select>
			  			</td>
			  			<td>
			  				<select name="staff" id="staff">
			  					@foreach($staffs as $staff)
			  						<option value="{{ $staff->id }}" {{ ($staff->id === $ra->fk_staff) ? "selected" : "" }}>{{ $staff->name }}</option>
			  					@endforeach
			  				</select>
			  			</td>
			  			<td>
			  				{{ Form::email('email', $ra->email) }} <br>
			  				{{ Form::text('phone', $ra->phone) }} <br>
			  				{{ Form::text('room', $ra->room) }}
			  			</td>
			  			<td>
			  				{{Form::submit('Update', ['class' => 'btn btn-info'])}}
			  				{{ Form::close() }}
			  				{{ Form::open(['url' => '/users/' . $ra->id, 'method' => 'delete']) }}
			  					{{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
			  				{{ Form::close() }}
			  			</td>
			  			
			  		</tr>

			  		<!-- Otherwise -->
			  		@else
			  		<tr>
			  			<td>{{ $ra->first_name . ' ' . $ra->last_name }}</td>
			  			<td>{{ $ra->role }}</td>
			  			<td>{{ $ra->staff }}</td>
			  			<td width="300" class="contact">
			  				<table>
			  					<tr>
			  						<td><span class="glyphicon glyphicon-envelope"></span> <a href="mailto:{{$ra->email}}">{{ $ra->email }}</a></td>
			  					</tr>
			  					<tr>
			  						<td><span class="glyphicon glyphicon-earphone"></span> {{ $ra->phone }}</td>
			  					</tr>
			  					<tr>
			  						<td><span class="glyphicon glyphicon-home"></span> {{ $ra->room }}</td>
			  					</tr>
			  				</table>
			  			</td>
			  			<td>
			  				<a href="/admin/users/edit/{{$ra->id}}#users">
			  					<span class="glyphicon glyphicon-pencil"></span>
			  				</a>
			  			</td>
			  		</tr>

			  		@endif
			  		@endforeach
			  		<tr>
			  			<td colspan="5">
			  				<a class="btn btn-success" href="/users/create">Add user</a>
			  			</td>
			  		</tr>
			  	</table>
			  </div>
			  <!-- /tab 3 -->
			  <!-- TAB 4 - EDIT ROLES -->
			  <div class="tab-pane" id="roles">
			      <table class="table table-striped table-hover">
			  		<tr>
			  			<th>Role ID</th>
			  			<th>Role</th>
			  			<th>Edit</th>
			  		</tr>
			  		@foreach($roles as $r)
			  		<tr>
			  			@if( isset($edit_role) && $r->id == $edit_role )
			  				{{ Form::open(['url' => "/roles/$r->id", 'method' => 'put']) }}
			  					<td>{{ $r->id }}</td>
			  					<td>{{ Form::text('role_name', $r->role_name) }}</td>
			  					<td>
			  					    {{ Form::submit('Update', ['class' => 'btn btn-info']) }}
			  					    {{ Form::open(['url' => '/roles/' . $r->id, 'method' => 'delete']) }}
			  					        {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
			  				        {{ Form::close() }}
			  					</td>
			  				{{ Form::close() }}
			  			@else
			  				<td>{{ $r->id }}</td>
			  				<td>{{ $r->role_name }}</td>
			  				<td>
			  					<a href="/admin/roles/edit/{{$r->id}}#roles">
			  						<span class="glyphicon glyphicon-pencil"></span>
			  					</a>
			  				</td>
			  			@endif

			  		</tr>
			  		@endforeach
			  		<tr>
			  			{{ Form::open(['url' => '/roles', 'method' => 'post']) }}
			  			<td></td>
			  			<td>{{ Form::text('role_name', '', ['placeholder' => 'Role Name']) }}</td>
			  			<td>{{ Form::submit('Add Role',['class' => 'btn btn-success']) }}</td>
			  			{{ Form::close() }}
			  		</tr>
			  	</table>
                
			  </div>
			  <!-- /tab -->
			</div>
		</div>
		<!-- /main content -->


	</div>
	<!-- /frame -->

	

<!-- VIEW ALL DUTY DAYS -->

@stop

@section('footer')
<!--
|-----------------------
| script to launch correct picker page
|-----------------------
-->
<script>
	// -----------
	// from github 
	$(document).ready(function(){
		// Javascript to enable link to tab
		var url = document.location.toString();
		if (url.match('#')) {
		    $('.nav-pills a[href=#'+url.split('#')[1]+']').tab('show') ;

		} 
		// Change hash for page-reload
		$('.nav-pills a').on('shown', function (e) {
		    window.location.hash = e.target.hash;
		})

		window.scrollTo(0,0);
	});
	// -----------

	$('#go').on('click', function(){
		var m = $('#m').val(),
			y = $('#y').val();

		console.log(m + '' + y);

		window.location.href = '/admin/duty/' + m + '/' + y;
	});
	
</script>

@stop
