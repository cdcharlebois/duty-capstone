@extends('layouts.main')

@section('header')
  <?php
    // this determines how far to offset the first day of the month
    $startdate = new DateTime($dutyDays[0]->date);
    $offset = $startdate->format('w');
    $push = 14.25 * $offset;
  ?>
  <style type="text/css">
    .last + li{
      margin-left: {{ $push . '%' }}
    }
    .cal > li:not(.title){
      width:14.25%;
      float:left;
      display:inline;
      background-color:#eee;
      min-height:125px;
      border:1px solid #888;
      /*padding:3px;*/
    }
    .cal > li.title{
      width:14.25%;
      float:left;
      display:inline;
      background-color:#aaa;
      color:#fff;
      text-align: center;
    }
    .list > li{
      list-style-type: none;
    }
    .ra-label{
      -webkit-box-sizing:border-box;
      box-sizing:border-box;
      padding:3px;
      font-size: 16px;
      background-color: #5CB85C;
      color:#fff;
      border-radius: 3px;
      text-align: center;
    }
    h4{
      background-color: #a9a9a9;
      color:#fff;
      margin:0;
      padding:3px;
    }
    .cal > li > * :not(h4){
      margin: 3px 6px;
    }
    .month{
      font-size: 32px;
    }
    .staff-1{
      background-color:#ffacac;
    }
    .staff-2{
      background-color:#acffac;
    }
    .staff-3{
      background-color:#acacff;
    }
    .staff-4{
      background-color:#ffffac;
    }
    .staff-5{
      background-color:#ffacff;
    }
    .staff{
      padding:2px;
    }
  </style>
@stop

@section('content')
  <?php
    $nextLink = $currMonth == 12 ? '/admin/duty/1/' . ($currYear+1) : '/admin/duty/' . ($currMonth+1) . '/' . $currYear;
    $prevLink = $currMonth == 1 ? '/admin/duty/12/' . ($currYear-1) : '/admin/duty/' . ($currMonth-1) . '/' . $currYear;
  ?>
  <ul class="pager">
    <li class="previous"><a href="{{ $prevLink }}">&larr; Previous Month</a></li>
    <li class="month">
      <?php
      $mos = ['', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
      echo "$mos[$currMonth], $currYear";
    ?>
    </li>
    <li class="next"><a href="{{ $nextLink }}">Next Month &rarr;</a></li>
  </ul>
    <div class="row">
      <div class="cal col-sm-7">
        <li class="title">Sunday</li>
        <li class="title">Monday</li>
        <li class="title">Tuesday</li>
        <li class="title">Wednesday</li>
        <li class="title">Thursday</li>
        <li class="title">Friday</li>
        <li class="title last">Saturday</li>
        @foreach ($dutyDays as $day)
          <li>
              <?php
                $foo = new DateTime($day->date);
              ?>
              <h4>{{ $foo->format('d') }}</h4>

              @foreach($day->ras as $ra)
                <div class="staff staff-{{ $ra->staff }}">{{ $ra->last_name }}</div>
              @endforeach

            <!-- </a> -->
          </li>
        @endforeach
      </div>

      <div class="col-sm-5">
        <h2>Key</h2>
        @foreach($staffs as $staff)
        <span class="staff staff-{{ $staff->id }}">
          {{ $staff->name }}
        </span>
        <br>
        @endforeach
      </div>
    </div>
@stop
