@extends('layouts.main')

@section('header')
	<?php
		// this determines how far to offset the first day of the month
		$startdate = new DateTime($dutyDays[0]->date);
		$offset = $startdate->format('w');
		$push = 14.25 * $offset;
	?>
	<style type="text/css">
		.last + li{
			margin-left: {{ $push . '%' }}
		}
		.cal > li:not(.title){
			width:14.25%;
			float:left;
			display:inline;
			background-color:#eee;
			height:125px;
			border:1px solid #888;
			/*padding:3px;*/
		}
		.cal > li.title{
			width:14.25%;
			float:left;
			display:inline;
			background-color:#aaa;
			color:#fff;
			text-align: center;
		}
		.list > li{
			list-style-type: none;
		}
		.ra-label{
			-webkit-box-sizing:border-box;
			box-sizing:border-box;
			padding:3px;
			font-size: 16px;
			background-color: #5CB85C;
			color:#fff;
			border-radius: 3px;
			text-align: center;
		}
		h4{
			background-color: #a9a9a9;
			color:#fff;
			margin:0;
			padding:3px;
		}
		.cal > li > * :not(h4){
			margin: 3px 6px;
		}
		.month{
			font-size: 32px;
		}
	</style>
@stop

@section('content')
	<?php
		$nextLink = $currMonth == 12 ? '/admin/duty/1/' . ($currYear+1) : '/admin/duty/' . ($currMonth+1) . '/' . $currYear;
		$prevLink = $currMonth == 1 ? '/admin/duty/12/' . ($currYear-1) : '/admin/duty/' . ($currMonth-1) . '/' . $currYear;
	?>
	<ul class="pager">
	  <li class="previous"><a href="{{ $prevLink }}">&larr; Previous Month</a></li>
	  <li class="month">
	  	<?php
			$mos = ['', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
			echo "$mos[$currMonth], $currYear";
		?>
	  </li>
	  <li class="next"><a href="{{ $nextLink }}">Next Month &rarr;</a></li>
	</ul>

	<table class="table">
		<tr>
			<th>Date</th>
			<th>RAs</th>
		</tr>
<!-- 		/*
		[
			{
				->date 	:string
				->ras 	:[
							:string
						]
			}
		]
		*/ -->
		@foreach($dutyDays as $m)
		<tr>
			<td>{{$m->date}}</td>
			<td>{{$m->first_name . ' ' . $m->last_name}}</td>
		</tr>
		@endforeach
	</table>
@stop
