@extends('layouts.main')

@section('header')
<style type="text/css">
	.month{
      font-size: 32px;
    }
</style>
@stop

@section('content')

<!--
[
	{
		->date 	:string
		->ras 	:[
						{
								->first_name		:string
								->last_name		 	:string
								->staff				:string
						}
				]
	}
]
-->
<!-- ideal structure
[
  {
    -> date   :string
    -> staff  :string
    -> ras    :[
      {
        ->first_name :string
        ->last_name  :string
      }  
    ]
  }
]
-->
<?php
    $nextLink = $currMonth == 12 ? '/admin/duty/1/' . ($currYear+1) : '/admin/duty/' . ($currMonth+1) . '/' . $currYear;
    $prevLink = $currMonth == 1 ? '/admin/duty/12/' . ($currYear-1) : '/admin/duty/' . ($currMonth-1) . '/' . $currYear;
  ?>
  <ul class="pager">
    <li class="previous"><a href="{{ $prevLink }}">&larr; Previous Month</a></li>
    <li class="month">
      <?php
      $mos = ['', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
      echo "$mos[$currMonth], $currYear Master Calendar";
    ?>
    </li>
    <li class="next"><a href="{{ $nextLink }}">Next Month &rarr;</a></li>
  </ul>
  <div class="main">
  	<table class="table-striped table table-hover">
  		<tr>
  				<th>Date</th>
  			@foreach($staffs as $s)
  				<th>{{ $s->name }}</th>
  			@endforeach
  		</tr>
  		@foreach($dutyDays as $d)
  			<tr>
  				<td>{{ $d->date }}</td>
  				<td>
  					@for($i = 0; $i < count($d->ras); $i++)
  						@if($i > 0 && $d->ras[$i]->staff != $d->ras[$i-1]->staff)
  							</td><td>
  						@endif
  						{{ $d->ras[$i]->last_name }} <br>
  					@endfor
  				</td>
  			</tr>
  		@endforeach
  	</table>
  </div>
@stop