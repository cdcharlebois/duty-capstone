@extends('layouts.main')

@section('content')
	<div class="well">
		<h1>Login</h1>
		{{ Form::open(['route' => 'sessions.store']) }}
		<table class="table">
			<tr>
				<td>{{ Form::label('email', 'Email: ') }}</td>
				<td>{{ Form::text('email') }}</td>
			</tr>
			<tr>
				<td>{{ Form::label('password', 'Password: ') }}</td>
				<td>{{ Form::password('password') }}</td>
			</tr>
			<tr>
				<td></td>
				<td>{{ Form::submit() }}</td>
			</tr>
		</table>

		{{ Form::close() }}	
	</div>
	
@stop