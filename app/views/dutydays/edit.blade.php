@extends('layouts.main')

@section('content')

{{ Form::open(['url' => '/dutydays/' . $d->id, 'method' => 'post']) }}

<?php
	$thisDay = new DateTime($d->date);
	$day = $thisDay->format('j');
	$month = $thisDay->format('n');
	$year = $thisDay->format('Y');
?>

<h1>{{ $d->date }}</h1>
<div class="row">
	<div class="col-sm-6">
		<table class="table">
			<tr>
				<td>Staff:</td>
				<td>{{ $staff->name }}</td>
			</tr>
			<tr>
				<td>Number of RAs:</td>
				<td>
					<select name="numras" id="numras">
						<option value="0" {{($d->number_of_ras == 0 )? 'selected' : ''}}>0</option>
						<option value="1" {{($d->number_of_ras == 1 )? 'selected' : ''}}>1</option>
						<option value="2" {{($d->number_of_ras == 2 )? 'selected' : ''}}>2</option>
					</select>
					{{ Form::submit('Update', ['class' => 'btn btn-info']) }}
					{{ Form::close() }}
				</td>
			</tr>
			<tr>
				<td>RAs:</td>
				<td>
					@foreach($ras as $ra)
						{{ Form::open(['url' => '/picks/' . $ra->pick, 'method' => 'put']) }}
							<select name="ra" id="ra">
								@foreach($allras as $a)
									<option value="{{$a->id}}" {{($ra->ra == $a->id )? 'selected' : ''}}>{{ $a->first_name . ' ' . $a->last_name }}</option>
								@endforeach	
							</select>
							{{ Form::submit('Update', ['class' => 'btn btn-info']) }}
						{{ Form::close() }}
					@endforeach
				</td>
			</tr>
			<tr>
				<td colspan="2"><a href="/picks/{{$month}}/{{$year}}">Return to Duty Picking</a></td>
			</tr>
		</table>

	</div>
</div>



@stop