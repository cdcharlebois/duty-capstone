@extends('layouts.main')

@section('content')

<?php
	$thisDay = new DateTime($d->date);
	$day = $thisDay->format('j');
	$month = $thisDay->format('n');
	$year = $thisDay->format('Y');
?>

<h1>{{ $d->date }}</h1>
<div class="row">
	<div class="col-sm-6">
		<table class="table">
			<tr>
				<td>Staff:</td>
				<td>{{ $staff->name }}</td>
			</tr>
			<tr>
				<td>Number of RAs:</td>
				<td>{{ $d->number_of_ras }}</td>
			</tr>
			<tr>
				<td>RAs:</td>
				<td>
					@foreach($ras as $ra)
						{{ $ra->first_name . ' ' . $ra->last_name }} <br>
					@endforeach
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<a href="/dutydays/{{$d->id}}/edit" class="btn btn-primary">Edit</a>
				</td>
			</tr>
			<tr>
				<td colspan="2"><a href="/picks/{{$month}}/{{$year}}">Return to Duty Picking</a></td>
			</tr>
		</table>

	</div>
</div>


@stop