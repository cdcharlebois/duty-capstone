@extends('layouts.main')

@section('content')
	<?php
		$ra = Auth::user();
	?>
	<h1>{{$ra->first_name . ' ' . $ra->last_name}}</h1>
	<br>
	{{ Form::open(['url' => '/users/' . $ra->id, 'method' => 'put']) }}
	<table>
		<tr>
			<td>{{ Form::label('first_name', 'First Name: ') }}</td>
			<td>{{ Form::text('first_name', $ra->first_name) }}</td>
			<td></td>
		</tr>
		<tr>
			<td>{{ Form::label('last_name', 'Last Name: ') }}</td>
			<td>{{ Form::text('last_name', $ra->last_name) }}</td>
			<td></td>
		</tr>
		<tr>
			<td>{{ Form::label('email', 'Email: ') }}</td>
			<td>{{ Form::text('email', $ra->email) }}</td>
			<td></td>
		</tr>
		<tr>
			<td>{{ Form::label('phone', 'Phone: ') }}</td>
			<td>{{ Form::text('phone', $ra->phone, ['placeholder' => 'Ex. 123-456-7890']) }}</td>
			<td></td>
		</tr>
		<tr>
			<td>{{ Form::label('roome', 'Room: ') }}</td>
			<td>{{ Form::text('room', $ra->room, ['placeholder' => 'Ex. FAW 101']) }}</td>
			<td></td>
		</tr>
		<tr>
			<td>{{ Form::label('staff', 'Staff: ') }}</td>
			<td>
				<select name="staff" id="staff" disabled>
					@foreach($staffs as $staff)
						<option value="{{ $staff->id }}" {{ ($staff->id === $ra->fk_staff) ? "selected" : "" }}>{{ $staff->name }}</option>
					@endforeach
				</select>
			</td>
			<td></td>
		</tr>
		<tr>
			<td>{{ Form::label('role', 'Role: ') }}</td>
			<td>
				<select name="role" id="role" disabled>
					@foreach($roles as $role)
						<option value="{{ $role->id }}" {{ ($role->id === $ra->fk_role) ? "selected" : "" }}>{{ $role->role_name }}</option>
					@endforeach
				</select>
			</td>
			<td></td>
		</tr>
		<tr>
			<td colspan="3"><a id="showPass" href="#">Change my password</a></td>
		</tr>
		</table>
		<!-- password -->
		<table id="pass" class="bg-warning">
		<tr>
			<td>{{ Form::label('opwd', 'Old Password:') }}</td>
			<td>{{ Form::password('opwd') }}</td>
			<td></td>
		</tr>
		<tr>
			<td>{{ Form::label('npwd1', 'New Password:') }}</td>
			<td>{{ Form::password('npwd1') }}</td>
			<td></td>
		</tr>
		<tr>
			<td>{{ Form::label('npwd2', 'Confirm New Password: ') }}</td>
			<td>{{ Form::password('npwd2') }}</td>
			<td id="feedback"></td>
		</tr>
		</table>
	
	{{ Form::submit('Update', ['class' => 'btn btn-info']) }}
	{{ Form::close() }}
@stop


@section('footer')
<script type="text/javascript">
	$(function(){
		$('#npwd2').on('keyup', function(e){
			var fb = $('#feedback');
			if ( $('#npwd2').val() != $('#npwd1').val() ){
				fb.addClass('text-danger');
				fb.text("Passwords don't match :(");
			}
			else {
				fb.removeClass('bg-danger');
				fb.addClass('text-success');
				fb.text('Passwords match :)');
			}
		});
		$('#pass').css('display', 'none');
		$('#showPass').on('click', function(){
			$('#pass').fadeToggle();
		});
	});
</script>
@stop












