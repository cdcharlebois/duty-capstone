@extends('layouts.main')

@section('content')

{{ Form::model($user, array('route' => array('users.update', $user->id))) }}

<table class="">
		<tr>
			<td>{{ Form::label('first_name', 'First Name: ') }}</td>
			<td>{{ Form::text('first_name') }}</td>
			<td></td>
		</tr>
		<tr>
			<td>{{ Form::label('last_name', 'Last Name: ') }}</td>
			<td>{{ Form::text('last_name') }}</td>
			<td></td>
		</tr>
		<tr>
			<td>{{ Form::label('email', 'Email: ') }}</td>
			<td>{{ Form::text('email') }}</td>
			<td></td>
		</tr>
		<tr>
			<td>{{ Form::label('staff', 'Staff: ') }}</td>
			<td>
				<select name="staff" id="staff">
					@foreach($staffs as $staff)
						<option value="{{ $staff->id }}">{{ $staff->name }}</option>
					@endforeach
				</select>
			</td>
			<td></td>
		</tr>
		<tr>
			<td>{{ Form::label('role', 'Role: ') }}</td>
			<td>
				<select name="role" id="role">
					@foreach($roles as $role)
						<option value="{{ $role->id }}">{{ $role->role_name }}</option>
					@endforeach
				</select>
			</td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td>{{ Form::submit() }}</td>
			<td></td>
		</tr>
	</table>

{{ Form::close() }}
@stop