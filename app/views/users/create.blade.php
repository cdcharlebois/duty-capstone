@extends('layouts.main')

@section('content')
	<h1>Create User</h1>
	<br>
	{{ Form::open(['route' => 'users.store']) }}
	<table class="">
		<tr>
			<td>{{ Form::label('first_name', 'First Name: ') }}</td>
			<td>{{ Form::text('first_name') }}</td>
			<td></td>
		</tr>
		<tr>
			<td>{{ Form::label('last_name', 'Last Name: ') }}</td>
			<td>{{ Form::text('last_name') }}</td>
			<td></td>
		</tr>
		<tr>
			<td>{{ Form::label('email', 'Email: ') }}</td>
			<td>{{ Form::text('email') }}</td>
			<td></td>
		</tr>
		<tr>
			<td>{{ Form::label('phone', 'Phone: ') }}</td>
			<td>{{ Form::text('phone', null, ['placeholder' => 'Ex. 123-456-7890']) }}</td>
			<td></td>
		</tr>
		<tr>
			<td>{{ Form::label('room', 'Room: ') }}</td>
			<td>{{ Form::text('room', null, ['placeholder' => 'Ex. FAW 101']) }}</td>
			<td></td>
		</tr>
		<tr>
			<td>{{ Form::label('password', 'Password: ') }}</td>
			<td>{{ Form::password('password') }}</td>
			<td></td>
		</tr>
		<tr>
			<td>{{ Form::label('staff', 'Staff: ') }}</td>
			<td>
				<select name="staff" id="staff">
					@foreach($staffs as $staff)
						<option value="{{ $staff->id }}">{{ $staff->name }}</option>
					@endforeach
				</select>
			</td>
			<td></td>
		</tr>
		<tr>
			<td>{{ Form::label('role', 'Role: ') }}</td>
			<td>
				<select name="role" id="role">
					@foreach($roles as $role)
						<option value="{{ $role->id }}">{{ $role->role_name }}</option>
					@endforeach
				</select>
			</td>
			<td></td>
		</tr>
	</table>
	<input type="submit" class="btn btn-primary" value="create" />
	{{ Form::close() }}
@stop