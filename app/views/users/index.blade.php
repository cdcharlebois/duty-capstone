@extends('layouts.main')

<!--
	LOCALS:
		$staffs: 	hashmap of staffs' PKs mapped to their names
		$ras: 		Users::get()
-->

@section('footer')
<script type="text/javascript">
// $(function(){
// 	$('.confirm').css('display','none');
// });
</script>
@stop

@section('content')
<h1>{{ $staffName }} Staff</h1>
<table class="table table-striped">
	<tr>
		<th>Name</th>
		<th>Email</th>
		<th>Phone</th>
		<th>Room</th>
		@if(Auth::user()->fk_role > 1)
		<th>Edit</th>
		@endif
	</tr>
	@foreach($ras as $ra)
	
	<!-- If this RA is being edited, show a pre-populated form -->
	@if (isset($edit) && $edit == $ra->id)
	<tr>
		{{ Form::open(['url' => '/users/' . $ra->id, 'method' => 'put']) }}
		<td>
			{{ Form::text('first_name', $ra->first_name) }}
			{{ Form::text('last_name', $ra->last_name) }}
		</td>
		<td>
			{{ Form::email('email', $ra->email)}}
		</td>
		<td>
			{{ Form::text('phone', $ra->phone) }}
		</td>
		<td>
			{{ Form::text('room', $ra->room) }}
		</td>
		<td>{{Form::submit('Update',['class' => 'btn btn-info'])}}</td>
		{{ Form::close() }}
	</tr>

	<!-- Otherwise -->
	@else
	<tr>
		<td>{{ $ra->first_name . ' ' . $ra->last_name }}</td>
		<td>{{ $ra->email }}</td>
		<td>{{ $ra->phone }}</td>
		<td>{{ $ra->room }}</td>
		@if (Auth::user()->fk_role == 2 && $ra->fk_staff == Auth::user()->fk_staff || Auth::user()->fk_role == 5)
		<td>
			<a href="/users/{{$ra->id}}/edit">
				<span class="glyphicon glyphicon-pencil"></span>
			</a>
		</td>
		@endif
	</tr>

	@endif
	@endforeach
	@if(Auth::user()->fk_role > 1)
	<tr>
		<td colspan="6">
			<a class="btn btn-success" href="/users/create">Add User</a>
		</td>
	</tr>
	@endif
</table>

@stop