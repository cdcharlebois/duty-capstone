<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	Route::filter('ra', function(){
		if (Auth::user()->fk_role == 1) // if the user is an RA
		{
			return Redirect::to('/')->with('message', ['type'=>'warning', 'text'=>'Sorry, you don\'t have rights to access that'] );
		}
	});
});


App::after(function($request, $response)
{
	//
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	if (Auth::guest()) return Redirect::guest('login');
});


Route::filter('auth.basic', function()
{
	return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});



/*
|--------------------------------------------------------------------------
| My Filters
|--------------------------------------------------------------------------
|
| Role 1 (RA)
| -----------
| * view staff information
| * view staff schedule
| * view/add/edit switch proposals
|
| Role 2 (HRA)
| ------------
| * view/add/edit staff information
| * view/add/edit staff schedule
| * view/add/edit/confirm switch proposals
|
| ...
|
*/

Route::filter('auth_hra', function(){
	if (!Auth::user()) return Redirect::to('/login');
	else if(Auth::user()->fk_role < 2) return Redirect::to('/')->with('message', ['type'=>'warning', 'text'=>'Sorry, you don\'t have rights to access that']);
});

Route::filter('auth_admin', function()
{
	if (!Auth::user()) return Redirect::to('/login');
	else if(Auth::user()->fk_role != 5) return Redirect::to('/');
});


















