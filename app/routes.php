<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('hello');
});

Route::get('profile', function()
{
	//return "Hello, " . Session::get('user')->first_name;
	if (Auth::check()){
		return "Hello, " . Auth::user()->first_name;	
	}
	return "Please login";
	
});

Route::get('login', 'SessionsController@create');
Route::resource('sessions', 'SessionsController');

// Ensures the user is logged in to access these.
Route::group(['before' => 'auth'], function(){
	Route::get('logout', 'SessionsController@destroy');	
	Route::resource('picks', 'PicksController');
	Route::resource('users', 'UsersController');
	Route::get('/me', 'UsersController@display_profile');
	Route::resource('switches', 'SwitchesController', ['except' => ['show']]);
});

Route::group(['before' => 'auth_hra'], function(){
	// HRA ONLY
	Route::get('/picks/{month}/{year}', 'PicksController@pick');
	Route::get('/dutydays/{id}', 'DutyDayController@show');
	Route::get('/dutydays/{id}/edit', 'DutyDayController@edit');
	Route::post('/dutydays/{id}', 'DutyDayController@update');
	Route::get('/picks/{day}/oncall/{ra}', 'PicksController@add_oncall');
	Route::get('/switches/accepted', 'SwitchesController@accepted');
	Route::get('/switches/confirm/{sid}', 'SwitchesController@confirm');
	Route::get('/switches/deny/{sid}', 'SwitchesController@deny');
	Route::get('/users/create', 'UsersController@create');
	Route::post('/f/reset', 'PicksController@resetcounts');
});

// Admin Permissions
Route::group(['before' => 'auth_admin'], function(){
	Route::get('/admin', 'AdminController@index');
	Route::resource('staffs', 'StaffsController');
	Route::resource('roles', 'RolesController');
	Route::get('/admin/duty/{month}/{year}', 'AdminController@display_duty_days');
	Route::get('/admin/users/edit/{userId}', 'AdminController@admin_edit_user');
	Route::get('/admin/staffs/edit/{staffId}', 'AdminController@admin_edit_staff');
	Route::get('/admin/roles/edit/{roleId}', 'AdminController@admin_edit_role');
});
