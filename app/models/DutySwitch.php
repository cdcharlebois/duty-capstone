<?php

class DutySwitch extends Eloquent {

	protected $guarded = array();

	protected $table = 'switchs';

	protected $fillable = ['fk_user_from', 'fk_user_to', 'isConfirmed', 'fk_duty_day'];

	public static $rules = array();
}
