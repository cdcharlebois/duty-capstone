<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

        $this->call('RolesTableSeeder');
        $this->call('StaffsTableSeeder');
		$this->call('UserTableSeeder');
	}

}

class RolesTableSeeder extends Seeder {
    public function run()
    {
        DB::table('roles')->delete();

        Role::create([
            'id'            => 1,
            'role_name'     => 'RA'
            ]);

        Role::create([
            'id'            => 2,
            'role_name'     => 'HRA']);

        Role::create([
            'id'            => 5,
            'role_name'     => 'admin']);
    }
}

class StaffsTableSeeder extends Seeder {
    public function run()
    {
        DB::table('staffs')->delete();

        Staff::create([
            'id'            => 1,
            'name'          => 'Falcones & North Campus',
            'duty_phone'    => '555-555-5555'
            ]);
    }
}

class UserTableSeeder extends Seeder {

    public function run()
    {
        DB::table('users')->delete();

        User::create([
            'fk_role'       => '5',
            'fk_staff'      => '1',
            'first_name'    => 'Conner',
            'last_name'     => 'Charlebois',
            'room'          => 'FAW 101',
            'email'         => 'charleb_conn@bentley.edu',
            'password'      =>  Hash::make('1234'),
            'phone'         => '781-330-1240'
            ]);
        User::create([
        	'fk_role' 		=> '1',
        	'fk_staff' 		=> '1',
        	'first_name'	=> 'Alyssa',
        	'last_name'		=> 'Landolt',
            'room'          => 'FAW 201',
        	'email'			=> 'landolt_alys@bentley.edu',
        	'password'		=>  Hash::make('1234'),
            'phone'         => ''
        	]);
        User::create([
            'fk_role'       => '1',
            'fk_staff'      => '1',
            'first_name'    => 'Caroline',
            'last_name'     => 'Hulin',
            'room'          => 'FAN 107',
            'email'         => 'hulin_caro@bentley.edu',
            'password'      =>  Hash::make('1234'),
            'phone'         => ''
            ]);


    }

}