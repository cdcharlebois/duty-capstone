<?php

use Illuminate\Database\Migrations\Migration;

class AddPicksOncall extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::table('picks', function($table) 
		{
			$table->boolean('oncall');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::table('picks', function($table) 
		{
			$table->dropColumn('oncall');
		});
	}

}