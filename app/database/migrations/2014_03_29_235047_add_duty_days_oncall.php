<?php

use Illuminate\Database\Migrations\Migration;

class AddDutyDaysOncall extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::table('picks', function($table) 
		{
			$table->dropColumn('oncall');
		});

		Schema::table('duty_days', function($table)
		{
			$table->integer('oncall');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::table('duty_days', function($table) 
		{
			$table->dropColumn('oncall');
		});

		Schema::table('picks', function($table) 
		{
			$table->boolean('oncall');
		});
	}

}