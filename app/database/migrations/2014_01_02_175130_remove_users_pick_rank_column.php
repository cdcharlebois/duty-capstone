<?php

use Illuminate\Database\Migrations\Migration;

class RemoveUsersPickRankColumn extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::table('users', function($table) 
		{
			$table->dropColumn('pick_rank');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::table('users', function($table) 
		{
			$table->string('pick_rank');
		});
	}

}