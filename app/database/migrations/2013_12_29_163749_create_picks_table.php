<?php

use Illuminate\Database\Migrations\Migration;

class CreatePicksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('picks', function($table)
		{
			$table->increments('id');
			$table->integer('fk_duty_day')->unsigned();
			$table->integer('fk_user')->unsigned();
			$table->foreign('fk_duty_day')->references('id')->on('duty_days')->onDelete('cascade');
			$table->foreign('fk_user')->references('id')->on('users')->onDelete('cascade');
			$table->timestamps();
			$table->unique(array('fk_duty_day', 'fk_user'));
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('picks');
	}

}