<?php

use Illuminate\Database\Migrations\Migration;

class CreateSwitchsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('switchs', function($table)
		{
			$table->increments('id');
			$table->integer('fk_duty_day')->unsigned();
			$table->integer('fk_user_from')->unsigned();
			$table->integer('fk_user_to')->unsigned()->nullable();
			$table->boolean('isConfirmed')->default(false);
			$table->timestamps();
			$table->foreign('fk_duty_day')->references('id')->on('duty_days')->onDelete('cascade');
			$table->foreign('fk_user_from')->references('id')->on('users')->onDelete('cascade');
			$table->foreign('fk_user_to')->references('id')->on('users')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('switchs');
	}

}