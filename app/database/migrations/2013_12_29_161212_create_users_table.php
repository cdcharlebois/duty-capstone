<?php

use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// create users table
		Schema::create('users', function($table)
		{
			$table->increments('id');
			$table->integer('fk_role')->unsigned();
			$table->integer('fk_staff')->unsigned();
			$table->string('first_name');
			$table->string('last_name');
			$table->string('email')->unique();
			$table->string('password');
			$table->integer('pick_rank')->unique();
			$table->timestamps();
			$table->foreign('fk_role')->references('id')->on('roles')->onDelete('cascade');
			$table->foreign('fk_staff')->references('id')->on('staffs')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('users');
	}

}