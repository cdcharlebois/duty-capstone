<?php

use Illuminate\Database\Migrations\Migration;

class CreateDutyDaysTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('duty_days', function($table)
		{
			$table->increments('id');
			$table->integer('fk_staff')->unsigned();
			$table->date('date');
			$table->integer('number_of_ras');
			$table->timestamps();
			$table->foreign('fk_staff')->references('id')->on('staffs')->onDelete('cascade');
			$table->unique(array('fk_staff', 'date'));
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('duty_days');
	}

}