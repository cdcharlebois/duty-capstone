<?php

use Illuminate\Database\Migrations\Migration;

class CreateStaffsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// create staff table
		Schema::create('staffs', function($table)
		{
			$table->increments('id');			// _id 			-> PK
			$table->string('name')->unique();	// name 		-> staff name
			$table->string('duty_phone');		// duty_phone	-> duty phone number for this staff
			$table->timestamps();				// timestamps
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// drop
		Schema::drop('staffs');
	}

}