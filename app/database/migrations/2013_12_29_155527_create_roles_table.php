<?php

use Illuminate\Database\Migrations\Migration;

class CreateRolesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// create the roles table
		Schema::create('roles', function($table) {
        	$table->increments('id');				// autoincrement
        	$table->string('role_name')->unique();	// unique role_name
        	$table->timestamps();					// created_at and updated_at fields
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// drop roles
		Schema::drop('roles');
	}

}