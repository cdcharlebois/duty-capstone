<?php

use Illuminate\Database\Migrations\Migration;

class AddUsersPhone extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//add phone number field to user
		Schema::table('users', function($table) 
		{
			$table->string('phone');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::table('users', function($table) 
		{
			$table->dropColumn('phone');
		});
	}

}