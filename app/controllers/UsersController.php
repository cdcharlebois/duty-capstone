<?php

class UsersController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		/*
		|----------------------------------------
		| Breakdown for roles
		|----------------------------------------
		|
		| RA 	-> view: staff, edit: none
		| HRA 	-> view: staff, edit: staff
		| admin -> view: all, edit: all
		|
		*/
		//only show RAs and HRA
		$ras = User::where('fk_staff', '=', Auth::user()->fk_staff)
			->where('fk_role', '<=', 2)
			->get();

		$staffName = Staff::find(Auth::user()->fk_staff)->name;
		
		return View::make('users/index', [
			'ras'		=> $ras,
			'staffName'	=> $staffName
		]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		return View::make('users/create',[
			'roles' 	=> Role::get(),
			'staffs'	=> Staff::get()
		]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$data = [
			'first_name' => Input::get('first_name'),
			'last_name'  => Input::get('last_name'),
			'email'		 => Input::get('email'),
			'password'	 => Hash::make(Input::get('password')),
			'fk_staff'	 => Input::get('staff'),
			'fk_role'	 => Input::get('role'),
			'phone'		 => Input::get('phone'),
			'room'		 => Input::get('room')
		];

		User::create([
			'first_name'	=> $data['first_name'],
			'last_name'		=> $data['last_name'],
			'email'			=> $data['email'],
			'password'		=> $data['password'],
			'fk_staff'		=> $data['fk_staff'],
			'fk_role'		=> $data['fk_role'],
			'phone'			=> $data['phone'],
			'room'			=> $data['room']
		]);

		$newUser = $data['first_name'] . ' ' . $data['last_name'];

		return Redirect::to('/users')->with('message', ['type'=>'success','text'=>"User $newUser was created."]);
	}

	/**
	 * UNUSED 
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
			$ras = User::where('fk_staff', '=', Auth::user()->fk_staff)
			->where('fk_role', '<=', 2)
			->get();

		
		$staffName = Staff::find(Auth::user()->fk_staff)->first()->name;

		return View::make('users/index', [
			'ras'		=> $ras,
			'staffName' => $staffName,
			'edit'		=> $id
		]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$data = Input::all();
		$user = User::find($id);
		$user->first_name 	= $data['first_name'];
		$user->last_name 	= $data['last_name'];
		$user->email 		= $data['email'];
		$user->phone		= $data['phone'];
		$user->room 		= $data['room'];
		if (isset($data['staff']))
		{
			$user->fk_staff	= $data['staff'];
		}
		if (isset($data['role']))
		{
			$user->fk_role = $data['role'];
		}
		$oldPassMatch = (isset($data['opwd']) && Hash::check($data['opwd'], Auth::user()->password));
		$newPassSet = (isset($data['npwd1']) && isset($data['npwd2']));
		$newPassMatch = ($newPassSet && $data['npwd1'] === $data['npwd2']);
		$pwdWasReset = false;
		if ( $oldPassMatch && $newPassSet && $newPassMatch )
		{
			$user->password = Hash::make($data['npwd1']);
			$pwdWasReset = true;
		}
		$user->save();

		if (str_contains(URL::previous(), 'admin') && Auth::user()->fk_role === 5){
			return Redirect::to('/admin#users');
		}
		else if (str_contains(URL::previous(), '/me')){
			if ($pwdWasReset)
				return Redirect::to('/me')->with('message', ['type'=>'success', 'text'=>'Your profile has been updated and your password has been changed.']);
			else
				return Redirect::to('/me')->with('message', ['type'=>'success', 'text'=>'Your profile has been updated, but your password remains the same.']);
		}
		return Redirect::to('/users');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
		User::destroy($id);
		return Redirect::to('/admin#users');
	}

	public function display_profile(){
		$staffs = Staff::get();
		$roles  = Role::get();
		return View::make('users/me')->with(['staffs' => $staffs, 'roles' => $roles]);
	}

}