<?php

class PicksController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data = self::gather_data();
		return View::make('picks.rangepicker', $data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$dutyDayId = Input::get('dutyDayId');
		$userId = Input::get('userId');

		$temp = User::find($userId)->first_name;
			try{
				Pick::create([
					'fk_duty_day' 	=> $dutyDayId,
					'fk_user'		=> $userId
				]);
			}
			catch(Exception $e){
				$m = Session::get('month');
				$y = Session::get('year');
				return Redirect::to("/picks/{$m}/{$y}")
					->with('message', ['type' => 'warning', 'text' => "$temp is already on duty that day!" ]);
			}

		//	return "Could not create duty day for $temp. This day is full.";
		return Redirect::action('PicksController@pick', [
			'month' => Session::get('month'),
			'year' => Session::get('year')
		]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
		$pick = Pick::find($id)->first();
		$day = DutyDay::find($pick->fk_duty_day)->first();
		$user = User::find($pick->fk_user)->first();

		$data = [
			'date'		=> $day->date,
			'ra'		=> $user->first_name . ' ' . $user->last_name
		];
		return $data;
	}

	/**
	 * UNUSED
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//this is only used in an administrator override
		$pick = Pick::find($id);
		$newUser = Input::get('ra');
		try{
			$pick->fk_user = $newUser;
			$pick->save();
			$dutyday = DutyDay::find($pick->fk_duty_day);
			$dutyday->oncall = 0;
			$dutyday->save();
			return Redirect::to('/dutydays/' . $pick->fk_duty_day);	
		}
		catch(Exception $e){
			$name = User::find($newUser)->first_name;
			return Redirect::to('/dutydays/' . $pick->fk_duty_day)->with('message', ['text' => "Oops, it looks like $name is already on that day.", 'type' => 'danger']);
		}
		
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


	/**
	 * function pick($month, $year)
	 *
	 * @param int $month, int $year
	 * @return Response
	 *
	 * process
	 * -------
	 * + check the number of weekends reamaining
	 * + if > 0, only RAs pick, only weekends available
	 * + if = 0, all pick, all days available
	 * + if no days remain, no picks
	 *
	 */
	public function pick($month, $year)
	{
		$staff = Auth::user()->fk_staff;
		if (static::initializeMonth($staff, $month, $year))
		{
			Session::put('month', $month);
			Session::put('year', $year);

			// How many weekends are left?
			$weekendsLeft = DB::select(DB::raw(
				"select count(*) as number
					from duty_days d
					where number_of_ras = 2
						and month(d.date) = $month
						and year(d.date) = $year
						and d.fk_staff = $staff
						and number_of_ras > (select count(*)
							from picks p, duty_days d2
							where d2.date = d.date
								and p.fk_duty_day = d2.id
								and d2.fk_staff = $staff)"
			));

			$daysLeft = DB::select(DB::raw(
				"select count(*) as number
					from duty_days d
					where month(d.date) = $month
						and year(d.date) = $year
						and d.fk_staff = $staff
						and number_of_ras > (select count(*)
							from picks p, duty_days d2
							where d2.date = d.date
								and p.fk_duty_day = d2.id
								and d2.fk_staff = $staff)"
			));

			if ($weekendsLeft[0]->number > 0){
				Session::put('state', 'WE');
			}
			else if ($daysLeft[0]->number > 0){
				Session::put('state', 'WD');
			}
			else {
				Session::put('state', 'NONE');
			}

			# If it's the weekend phase, remove the HRA
			if (Session::get('state') === 'WE'){
				//  ----------------------------------------------
				//	Everything here works!!
				//	-------------VVV---------------------
				$picklist = User::whereRaw("fk_staff = $staff and fk_role < 2")->orderBy('id', 'asc')->get();
				$we_min = 999;
				$eligiblePickers = [];
				foreach ($picklist as $ra){
					//compute we_count
					$weekends = DB::select(DB::raw(
					"select count(*) as c
					from picks p, duty_days d
					where p.fk_duty_day = d.id and
						p.fk_user = $ra->id and
						d.number_of_ras = 2"
					));
					$ra->we_count = $weekends[0]->c;
					$weekdays = DB::select(DB::raw(
					"select count(*) as c
					from picks p, duty_days d
					where p.fk_duty_day = d.id and
						p.fk_user = $ra->id and
						d.number_of_ras = 1"
					));
					if ($ra->we_count < $we_min){
						unset($eligiblePickers);
						$eligiblePickers = [];
						array_push($eligiblePickers, $ra);
						$we_min = $ra->we_count;
					}
					else if ($ra->we_count == $we_min){
						array_push($eligiblePickers, $ra);
					}
					//...
				}
				$iChooseYou = rand(0, count($eligiblePickers)-1);
				Session::put('picker', $eligiblePickers[$iChooseYou]->id);
				//----------------^^^--------------------------------
			}
			else{
				// $picklist = User::where('fk_staff', '=', $staff)->where('fk_role', '<=', 2)->orderBy('id', 'asc')->get();
				$picklist = User::whereRaw("fk_staff = $staff and fk_role <= 2")->orderBy('id', 'asc')->get();
				$wd_min = 999;
				$eligiblePickers = [];
				foreach ($picklist as $ra){
					//compute we_count
					$weekends = DB::select(DB::raw(
					"select count(*) as c
					from picks p, duty_days d
					where p.fk_duty_day = d.id and
						p.fk_user = $ra->id and
						d.number_of_ras = 2"
					));
					$ra->we_count = $weekends[0]->c;
					$weekdays = DB::select(DB::raw(
					"select count(*) as c
					from picks p, duty_days d
					where p.fk_duty_day = d.id and
						p.fk_user = $ra->id and
						d.number_of_ras = 1"
					));
					$ra->wd_count = $weekdays[0]->c;
					if ($ra->wd_count < $wd_min){
						unset($eligiblePickers);
						$eligiblePickers = [];
						array_push($eligiblePickers, $ra);
						$wd_min = $ra->wd_count;
					}
					else if ($ra->wd_count == $wd_min){
						array_push($eligiblePickers, $ra);
					}
					//...
				}
				$iChooseYou = rand(0, count($eligiblePickers)-1);
				Session::put('picker', $eligiblePickers[$iChooseYou]->id);

			}
			

			// fetch duty days for the current month and belonging to the staff
			$dutyDays = DutyDay::whereRaw("fk_staff = $staff and month(date) = $month and year(date) = $year")->get();

			//insert the RAs
			foreach ($dutyDays as $day) {
				$foo = DB::table('duty_days')
					->join('picks', 'duty_days.id', '=', 'picks.fk_duty_day')
					->join('users', 'picks.fk_user', '=', 'users.id')
					->where('picks.fk_duty_day', '=', $day->id)
					->get();
				$bar = [];
				foreach ($foo as $ra) {
					array_push($bar, $ra);
				}
				$day->ras = $bar;
			}

			//pass the current staff
			$staff = Staff::find(Auth::user()->fk_staff);

			//pass the picklist and the duty days to the view.
			return View::make('picks/index', [
				'picklist' 	=> $picklist,
				'dutyDays'	=> $dutyDays,
				'currMonth' => $month,
				'currYear'  => $year,
				'staff'     => $staff,
				'picker'    => User::find(Session::get('picker'))->first_name
			]);

		}
		else
			return 'There was an error.';
	}

	public static function initializeMonth ($staff, $month, $year)
	{
		$month_length = static::days_in_month($month, $year);

		for ($i=1; $i <= $month_length; $i++) {
			$date = new DateTime("$month/$i/$year");

			//number of RAs on weekends is 2
			if($date->format('w') > 3)
			{
				$num = 2;
			}
			else
				$num = 1;

			//insert new rows
			try {
				DutyDay::create([
					'date'			=> $date,
					'number_of_ras'	=> $num,
					'fk_staff'		=> $staff
				]);
			} catch (Exception $e) {
			}
		}
		return true;
	}

	public static function add_oncall($day, $ra)
	{
		$ra = User::find($ra);
		$dutyDay = DutyDay::find($day);
		$dutyDay->oncall = $ra->id;
		$dutyDay->save();
		$thisDate = new DateTime($dutyDay->date);
		$month = $thisDate->format('n');
		$year = $thisDate->format('Y');
		return Redirect::to("picks/{$month}/{$year}");
	}

	private function gather_data(){
		// all distinct years in the DB
		$years = DB::table('duty_days')
                     ->select(DB::raw('distinct year(date) as year'))
                     ->get();

        // staff of logged-in user
        $staff = Staff::where('id', '=', Auth::user()->fk_staff)->first()->name;


        // +==============================================
        // | This populates the weekly schedule shown here
        // +----------------------------------------------
        date_default_timezone_set('America/Toronto');
        $today = new DateTime();
        $day = $today->format('j');
        $month = $today->format('n');
        $year = $today->format('Y');
        $staff_id = Auth::user()->fk_staff;

        $today_str = $year . '-' . $month . '-' . $day;
        // fetch duty days for the current month and belonging to the staff
			$nextWeekDuty = DutyDay::whereRaw("fk_staff = $staff_id
				and date >= CURDATE()")
					->orderBy('date', 'asc')
					->limit(7)
					->get();

			//insert the RAs
			foreach ($nextWeekDuty as $day) {
				$foo = DB::table('duty_days')
					->join('picks', 'duty_days.id', '=', 'picks.fk_duty_day')
					->join('users', 'picks.fk_user', '=', 'users.id')
					->where('picks.fk_duty_day', '=', $day->id)
					->get();
				$bar = [];
				foreach ($foo as $ra) {
					array_push($bar, [
						'first_name' => $ra->first_name,
						'last_name'	 => $ra->last_name,
					]);
				}
				$day->ras = $bar;
			}

		// My Duty Days
		// $justMyDutyDays = DB::table('duty_days')
		// 	->join('picks', 'duty_days.id', '=', 'picks.fk_duty_day')
		// 	->join('users', 'users.id', '=', 'picks.fk_user')
		// 	->where('picks.fk_user', '=', Auth::user()->id)
		// 	->orderBy('date')
		// 	->get();
		$me = Auth::user()->id;
		// $myDutyDaysWithPartners = DB::select(DB::raw(
		// 	"select d0.id as id, d0.date as date, u.first_name as partner
		// 	from duty_days d0, users u, picks p0
		// 	where u.id <> $me
		// 		and p0.fk_user = u.id
		// 		and p0.fk_duty_day = d0.id
		// 		and d0.id in 
		// 			(select d.id
		// 				from picks p, duty_days d
		// 				where p.fk_user = $me
		// 					and p.`fk_duty_day` = d.id)
		// 	order by d0.date"
		// ));
		$myNewDutyDays = DB::select(DB::raw(
			"select d0.id as id, d0.date as date, u.first_name as partner
			from duty_days d0, users u, picks p0
			where u.id <> $me
				and p0.fk_user = u.id
				and p0.fk_duty_day = d0.id
				and d0.id in 
					(select d.id
						from picks p, duty_days d
						where p.fk_user = $me
							and d.date > DATE_SUB(NOW(),INTERVAL 7 DAY)
							and p.`fk_duty_day` = d.id)
			UNION
			select d.id as id, d.date as date, null as parter
			from duty_days d, users u, picks p
			where u.id = $me 
				and p.fk_user = u.id 
				and p.fk_duty_day = d.id
				and d.date > DATE_SUB(NOW(), INTERVAL 7 DAY)
				and d.number_of_ras = 1
			order by DATE"
		));
		//something like the above method to add partners here, too
			

		// echo "<pre>";
		// print_r($myDutyDays);
		// echo "</pre>";

		return [
			'nextWeekDuty'	=> $nextWeekDuty,
			'years'			=> $years,
			'myStaff'		=> $staff,
			'currMonth'		=> $month,
			'currYear'		=> $year,
			// 'myDutyDays'	=> $justMyDutyDays,
			// 'myDutyDays'	=> $myDutyDaysWithPartners
			'myDutyDays'	=> $myNewDutyDays
		];
	}

	/////////////
	// helpers //
	/////////////
	/*
	 * days_in_month($month, $year)
	 * Returns the number of days in a given month and year, taking into account leap years.
	 *
	 * $month: numeric month (integers 1-12)
	 * $year: numeric year (any integer)
	 *
	 * Prec: $month is an integer between 1 and 12, inclusive, and $year is an integer.
	 * Post: none
	 */
	public static function days_in_month($month, $year)
	{
		// calculate number of days in a month
		return $month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year % 400 ? 28 : 29))) : (($month - 1) % 7 % 2 ? 30 : 31);
	}
	
	public function resetcounts(){
	    $staff = Input::get('staff');
	    // delete all picks duty days with fk_staff = $staff
	    DB::table('duty_days')->whereRaw("fk_staff = $staff")->delete();
	}

}
