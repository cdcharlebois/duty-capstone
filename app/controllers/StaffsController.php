<?php

class StaffsController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        return View::make('staffs.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return View::make('staffs.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		// POST to /store
		$data = [
			'name'	=> Input::get('name'),
			'phone'	=> Input::get('phone')
		];

		Staff::create([
			'name'			=> $data['name'],
			'duty_phone'	=> $data['phone']
		]);

		return Redirect::to('/admin');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        return View::make('staffs.show');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        return View::make('admins.index')
        	->with([
        		'staffs' 		=> Staff::all(),
        		'edit_staff'	=> $id
        	]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		// PUT staffs/{id}
		$data = Input::all();
		$staff = Staff::find($id);

		$staff->name 		= $data['name'];
		$staff->duty_phone 	= $data['phone'];
		$staff->save();

		if (str_contains(URL::previous(), 'admin') && Auth::user()->fk_role === 5){
			return Redirect::to('/admin#staffs');
		}

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
