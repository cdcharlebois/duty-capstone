<?php

class SwitchesController extends BaseController {

    // points to a page with all switches that have been accepted. i.e. 
    // Switches with both a user_to and a user_from, but that have not been 
    // confirmed by an administrator.
	public function accepted()
	{
		// [
		// 	{
		// 		->from_id
		// 		->from_name
		// 		->to_id 
		// 		->to_name
		// 		->day_id
		// 		->date
		// 	}
		// ]
		$os = [];
		$switches = DB::table('switchs')
			->whereRaw('fk_user_to is not null and isConfirmed = false')
			->get();
		foreach ($switches as $s) {
			$openSwitch = new StdClass;
			//switch
			$openSwitch->id 		= $s->id;
			//from
			$openSwitch->from_id 	= $s->fk_user_from;
			$u = User::find($s->fk_user_from);
			$openSwitch->from_name 	= $u->last_name;
			//to
			$openSwitch->to_id 		= $s->fk_user_to;
			$u = User::find($s->fk_user_to);
			$openSwitch->to_name 	= $u->last_name;
			//day
			$openSwitch->day_id 	= $s->fk_duty_day;
			$d = DutyDay::find($s->fk_duty_day);
			$openSwitch->date 		= $d->date;

			array_push($os, $openSwitch);

		}
		return View::make('switches.openSwitches')->with(['openSwitches' => $os]);
	}

	// confirms the switch in the DB, and updates the Picks
	public function confirm($sid)
	{
		//make the switch in the db
		$switch = DutySwitch::find($sid);
		try{
		    Pick::create([
			    'fk_duty_day' 	=> $switch->fk_duty_day,
		    	'fk_user'		=> $switch->fk_user_to
	    	]);
		}
		catch(Exception $e){
		    return Redirect::to('/switches/accepted')->with('message', ['text' => 'Switch could not be completed. ', 'type' => 'danger']);
		}

		$oldPick = DB::table('picks')
			->whereRaw("fk_duty_day = $switch->fk_duty_day and fk_user = $switch->fk_user_from")
			->get();
		Pick::destroy($oldPick[0]->id);
		
		//update the switch record
		$switch->isConfirmed = true;
		$switch->save();

		//remove the oncall
		$dutyDay = DutyDay::find($switch->fk_duty_day);
		$dutyDay->oncall = 0;
		$dutyDay->save();
		
		//flash info
		$newName = User::find($switch->fk_user_to)->first_name;
		$oldName = User::find($switch->fk_user_from)->first_name;
		$date = new DateTime($dutyDay->date);

		return Redirect::to('/switches/accepted')->with('message', ['text' => 'Switch completed successfully. ' . $newName . ' has taken ' . $oldName . '\'s duty on ' . $date->format('l \t\h\e jS') . '.', 'type' => 'success']);
	}
	
	public function deny($id)
    {
        DutySwitch::destroy($id);
        return Redirect::to('/switches/accepted')->with('message', ['text' => 'Switch denied.', 'type' => 'warning']);
    }

	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$me = Auth::user()->id;
		$mystaff = Auth::user()->fk_staff;
		
		// should return all duty days where fk_user_to is null and 
		// fk_user_from is not me and partner is not me
		// and staff is mine
		$newDutyDays = DB::select(DB::raw(
			"select s.id, d.date, u.first_name as partner
			from users u, duty_days d, picks p, switchs s
			where u.id = p.fk_user
				and p.fk_user <> s.fk_user_from
				and s.fk_duty_day = d.id
				and p.fk_duty_day = d.id
				and d.number_of_ras = 2
				and d.date > NOW()
				and u.id <> $me
				and d.fk_staff = $mystaff
				and s.id in 
						(select s.id
							from switchs s
							where s.fk_user_to is null
								and s.fk_user_from <> $me
								and s.isConfirmed = false)
			union
			select s.id, d.date, null as partner
			from users u, duty_days d, picks p, switchs s
			where u.id = p.fk_user
				and s.fk_duty_day = d.id
				and p.fk_duty_day = d.id
				and d.number_of_ras = 1
				and d.date > NOW()
				and d.fk_staff = $mystaff
				and s.id in 
						(select s.id
							from switchs s
							where s.fk_user_to is null
								and s.fk_user_from <> $me
								and s.isConfirmed = false)
			order by date"
		));
		$switches = DB::table('switchs')
			->whereRaw('fk_user_to is not null and isConfirmed = false')
			->count();
        
        Session::set('badge', count($newDutyDays));
        
        return View::make('switches.index')->with([
        	'dutydays' => $newDutyDays,
        	'numAccepted' => $switches
        ]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		// dutydays
		// [
		// 	{
		// 		->id
		// 		->partner 
		// 		->date
		// 	}
		//  ...
		// ]
		$me = Auth::user()->id;
		$newDutyDays = DB::select(DB::raw(
			"select d0.id as fk_duty_day, d0.date as date, u.first_name as partner
			from duty_days d0, users u, picks p0
			where u.id <> $me
				and p0.fk_user = u.id
				and p0.fk_duty_day = d0.id
				and d0.id in 
					(select d.id
						from picks p, duty_days d
						where p.fk_user = $me
							and d.date > NOW()
							and p.`fk_duty_day` = d.id)
				and d0.id not in (select fk_duty_day
									from switchs
									where isConfirmed = false)
			UNION
			select d.id as fk_duty_day, d.date as date, null as parter
			from duty_days d, users u, picks p
			where u.id = $me 
				and p.fk_user = u.id 
				and p.fk_duty_day = d.id
				and d.date > NOW()
				and d.number_of_ras = 1
				and d.id not in (select fk_duty_day
									from switchs
									where isConfirmed = false)
			order by DATE"
		));
        return View::make('switches.create')
            ->with(['dutydays' => $newDutyDays]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
		$data = Input::all();
		DutySwitch::create([
			'fk_user_from'	=> $data['from'],
			'fk_duty_day'	=> $data['day'],
			'isConfirmed'	=> false
		]);
		return Redirect::to('/switches/create')
		     ->with('message', ['text' => 'Your switch proposal has been sent. Fingers crossed!', 'type' => 'success']);;
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        return View::make('switches.show');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        return View::make('switches.edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
		$data = Input::all();
		$thisSwitch = DutySwitch::find($id);
		$thisSwitch->fk_user_to = $data['to'];
		$thisSwitch->save();


		return Redirect::to('/switches')->with('message', ['text' => 'Your acceptance has been submitted for HRA approval. Thanks!', 'type' => 'success']);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
