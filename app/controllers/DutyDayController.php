<?php

class DutyDayController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
		$thisDay = DutyDay::find($id);
		$staff = Staff::find($thisDay->fk_staff);
		//insert the RAs
		$dds = DB::table('duty_days')
				->join('picks', 'duty_days.id', '=', 'picks.fk_duty_day')
				->join('users', 'picks.fk_user', '=', 'users.id')
				->where('picks.fk_duty_day', '=', $id)
				->get();
		$ras = [];
		foreach ($dds as $ra) {
			array_push($ras, $ra);
		}
		return View::make('dutydays/show')
			->with([
				'd' 	=> $thisDay,
				'staff' => $staff,
				'ras'	=> $ras
			]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
		$thisDay = DutyDay::find($id);
		$staff = Staff::find($thisDay->fk_staff);
		$myRas = User::where('fk_staff', '=', Auth::user()->fk_staff)->where('fk_role', '<=', 2)->get();
		
		//
		$rasOnThisDay = DB::select(DB::raw(
			"select p.id as pick, u.id as ra, u.last_name, u.first_name
			 from picks p, users u 
			 where p.fk_duty_day = $id
			 	and u.id = p.fk_user"
		));



		return View::make('dutydays/edit')
			->with([
				'd' 	 => $thisDay,
				'staff'  => $staff,
				'ras'	 => $rasOnThisDay,
				'allras' => $myRas
			]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$thisDay = DutyDay::find($id);
		$thisStaff = $thisDay->fk_staff;
		$thisDate = $thisDay->date;
		
		// get the picks for this staff on this date
		$picks = DB::table('picks')
			->select('picks.id')
			->join('duty_days', 'duty_days.id', '=', 'picks.fk_duty_day')
			->where('duty_days.date', '=',  $thisDate)
			->where('duty_days.fk_staff', '=', $thisStaff)
			->orderBy('picks.updated_at')
			->get();
		if (Input::get('numras') == 0)
		{
			$thisDay->oncall = 0;
			foreach ($picks as $p) {
				try{
					Pick::destroy($p->id);
				}
				catch(Exception $e){}
			}
		}		
		else if (Input::get('numras') < $thisDay->number_of_ras){
			try{
				Pick::destroy($picks[1]->id);	
			}
			catch(Exception $e){}	
		}

		$thisDay->number_of_ras = Input::get('numras');
		$thisDay->save();
		return Redirect::to("/dutydays/$id");
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}