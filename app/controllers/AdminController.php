<?php

class AdminController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data = self::gather_data();
		
        return View::make('admins.index')
        	->with($data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return View::make('admins.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        return View::make('admins.show');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        return View::make('admins.edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function display_duty_days($month, $year)
	{
			$mDays = DB::select(DB::raw(
				"/* select all duty days in this month */
					select u.first_name, u.last_name, d.date, s.id as staff
					from users u, picks p, duty_days d, staffs s
						where p.fk_user = u.id
							and p.fk_duty_day = d.id
							and u.fk_staff = s.id
							and d.date in (select distinct d2.date
																from duty_days d2
																where month(d2.date) = $month
																	and year(d2.date) = $year)
					order by d.date, u.fk_staff"
				));
			/*
			[
				{
					->date 	:string
					->ras 	:[
									{
											->first_name		:string
											->last_name		 :string
											->staff				 :string
									}
							]
				}
			]
			*/
			$fDays = [];
			foreach($mDays as $d)
			{
				//if shift doesn't exist in fDays, add it
				$exists = -1;
				for ($i = 0; $i < count($fDays); $i++)
				{
					if ($fDays[$i]->date == $d->date) $exists = $i;
				}
				
				//create RA object
				$ra = new stdClass;
				$ra->first_name = $d->first_name;
				$ra->last_name = $d->last_name;
				$ra->staff = $d->staff;

				//if shift doesn't exist
				if ($exists < 0)
				{
					$dutyDate = new stdClass;
					$dutyDate->date = $d->date;
					$dutyDate->staff = $d->staff;
					$dutyDate->ras = [];
					array_push($dutyDate->ras, $ra);
					array_push($fDays, $dutyDate);
				}
				//add the RA to the date
				else
				{
					array_push($fDays[$exists]->ras, $ra);
				}


			}
			//echo "<pre>";
			//dd($fDays);


			//pass the picklist and the duty days to the view.
			return View::make('admins.masterSchedule', [
				'dutyDays'	=> $fDays,
				'currMonth'	=> $month,
				'currYear' => $year,
				'staffs'	=> Staff::get()
			]);


	}

	public function admin_edit_user($userId)
	{
		$data = self::gather_data();
		$data['edit'] = $userId;
		
        return View::make('admins.index')->with($data);
	}

	public function admin_edit_staff($staffId)
	{
		$data = self::gather_data();
		$data['edit_staff'] = $staffId;
		return View::make('admins.index')->with($data);
	}
	
	public function admin_edit_role($roleId)
    {
        $data = self::gather_data();
        $data['edit_role'] = $roleId;
        return View::make('admins.index')->with($data);
    }

	// +--------------------
	// | This function returns the most basic set of data needed for 
	// | 	displaying this page. Other routes may require additional
	// | 	data, but they add to those gathered by this function.
	// +--------------------
	private function gather_data(){

		$staffs = Staff::get();

		$years = DB::table('duty_days')
			->select(DB::raw('distinct year(date) as year'))
			->get();

		// for the Edit Users Portion:
		$ras = DB::table('users')->orderBy('fk_staff')->orderBy('fk_role', 'desc')->get();
		$roles = Role::get();
		
		foreach ($ras as $ra) {
			$ra->staff = Staff::where('id', '=', $ra->fk_staff)->first()->name;
			$ra->role = Role::where('id', '=', $ra->fk_role)->first()->role_name;
		}

		return [
			'staffs' 	=> $staffs,
			'years'		=> $years,
			'ras'		=> $ras,
			'roles'		=> $roles
		];
	}

}
