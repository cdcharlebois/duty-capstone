<?php

class SessionsController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        return View::make('sessions.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return View::make('sessions.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
		$input = Input::all();
		

		$attempt = Auth::attempt([
			'email'		=> $input['email'],
			'password' 	=> $input['password'] 
		]);

		if ($attempt) {
			$me = Auth::user()->id;
			$newDutyDays = DB::select(DB::raw(
			"select s.id, d.date, u.first_name as partner
			from users u, duty_days d, picks p, switchs s
			where u.id = p.fk_user
				and p.fk_user <> s.fk_user_from
				and s.fk_duty_day = d.id
				and p.fk_duty_day = d.id
				and d.number_of_ras = 2
				and d.date > NOW()
				and u.id <> $me
				and s.id in 
						(select s.id
							from switchs s
							where s.fk_user_to is null
								and s.fk_user_from <> $me
								and s.isConfirmed = false)
			union
			select s.id, d.date, null as partner
			from users u, duty_days d, picks p, switchs s
			where u.id = p.fk_user
				and s.fk_duty_day = d.id
				and p.fk_duty_day = d.id
				and d.number_of_ras = 1
				and d.date > NOW()
				and s.id in 
						(select s.id
							from switchs s
							where s.fk_user_to is null
								and s.fk_user_from <> $me
								and s.isConfirmed = false)
			order by date"
		));
        Session::set('badge', count($newDutyDays));
			return Redirect::to('/picks');
		}

		return Redirect::to('/login')->with('message', ['type'=>'danger', 'text'=>'Bad Password or User does not exist']);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        return View::make('sessions.show');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        return View::make('sessions.edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy()
	{
		//Session::forget('user');
		Auth::logout();
		return Redirect::to('/');
	}

}
